#!/usr/bin/env bash

target=$1

[ -z "$target" ] || [[ "$traget" =~ -{0,2}h(elp)? ]] && { echo >&2 "Usage: $0 prod|dev"; exit 0; }

case $target in
    "prod")
        target=$HOME/prod/studienplan5/web/dest
        ;;
    "dev")
        target=$HOME/Dokumente/dev/ruby/studienplan5/output-test
        ;;
    *)
        echo >&2 "No such target \"$target\"!"
        exit 1
        ;;
esac

for n in "$target"/{data,classes}.json; do ln -sf $n; done
