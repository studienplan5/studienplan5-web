// gulp itself
var gulp = require('gulp');

// sftp
var sftp = require('gulp-sftp');

// wiredep
var wiredep = require('wiredep').stream;
var mainBowerFiles = require('main-bower-files');

// gulp-handlebars
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var concat = require('gulp-concat');

// BrowserSync
var browserSync = require('browser-sync');
var reload = browserSync.reload;

//
// Variables
//

var bc = "bower_components/"

var dest = "dest";
var files = ["*.html", "css/*.css", "js/*.js"].concat(mainBowerFiles());

var copy = [
    // Bootstrap Glyphicons
    bc + "bootstrap/dist/fonts/*",
    // Fontawesome
    bc + "font-awesome/fonts/fontawesome-webfont.*",
    // German FullCalendar locale
    bc + "fullcalendar/dist/locale/de.js",
    // Images (GPL, AGPL)
    "img/*",
    // iCalendar files, also meta files
    "ics/**/*.ics", "ics/**/*.ical", "ics/.htaccess", "ics/indexes_header.html",
    // Copy JSON data files
    "data.json", "classes.json"
];

//
// Tasks
//

// default
gulp.task('default', ['copy', 'templates'], function() {
    gulp.src(files, {base: '.'})
    .pipe(wiredep())
    .pipe(gulp.dest(dest));
});

// copy files
gulp.task('copy', function(){
    gulp.src(copy, {base: '.'})
    .pipe(gulp.dest(dest));
})

// Pre-Render Handlebars templates
// 99% https://github.com/lazd/gulp-handlebars#gulpfilejs
gulp.task('templates', function(){
  gulp.src('templates/*.hbs')
    .pipe(handlebars())
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
      namespace: 'Templates',
      noRedeclare: true, // Avoid duplicate declarations
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest(dest, { base: '.' }));
});

// BrowserSync; Watch files for changes and reload
gulp.task('serve', ["default"], function() {
  browserSync({
    server: {
      baseDir: dest
    },
    reloadDelay: 500
  });

  gulp.watch([files].concat(['templates/*.hbs']), {cwd: "."}, ["reload"]);
});

// Reload BrowerSync browsers
gulp.task('reload', ["default"], function() {
    reload();
});
